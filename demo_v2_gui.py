# leer PLACAS y crear una base de datos
# usando el servidor :http://docs.platerecognizer.com/
# Api : https://api.platerecognizer.com/v1/plate-reader

########### Dependencies ##############
import PySimpleGUI as sg
import cv2
import numpy as np
import sys
from sys import exit as exit
from datetime import datetime
import requests
import pandas as pd
from tkinter import messagebox
import csv
import json
import psycopg2
from conexion import conecta
import datetime
import tkinter as tk
import cv2
from PIL import Image, ImageTk
from RegistroVehiculo import registro


# Parametros
max_num_plate=10 # maximo numero de placas a almacenar en el fichero .csv


#funcion para leer la placa
def leer_placa(img):
    regions = ['gb', 'it'] # estos parametros depende del tipo de placa a leer segun el pais
    # Se abre el archivo de datos .csv
    with open(img, 'rb') as fp:
        #se pide la consulta al servidor
        response = requests.post(
            'https://api.platerecognizer.com/v1/plate-reader/',
            data=dict(regions=regions),  # Opcional
            # se sube la foto al servidor
            # Se le envia el token a la APi de la web http://docs.platerecognizer.com/
            # Aqui tienes que colocar tu propio Token suscribiendote a la pagina
            files=dict(upload=fp),
            headers={'Authorization': 'Token 57a8615de92cdc9daa1e00e9cddf223f25fc0457 '})
    return response.json() #retorna el json con los datos procesados




'''
# funcion para validar y guardar la placa leida
def validar_placa(data, lista_placas,file,writer,max_num_plates, fechas):
    if data['results'] !=[]: #siempre que la imagen sea una placa
        #validar que no haya placas repetidas
        for result in data['results']:
            license_plate=result['plate']
        if len(lista_placas)>1: #para comenzar a verificar a partir de 1 placa
            if result['plate'] in lista_placas:
                validar=1 #filtra placas repetidas
            else:
                lista_placas.append(license_plate)
                validar=0 #asigna cero si no esta repetida
        else: # se agrega la primera placa
            lista_placas.append(license_plate)
            validar=0
        #verificar que haya menos de maximo numero de carros
        if len(lista_placas)>max_num_plates:
            validar=2
            lista_placas.pop() #elimina la ultima placa de la lista
        # se verifica si hay espacio en el parking o si esta repetida la placa antes de guardar
        guardar_placa(data, file, writer,validar)
        # Mensaje para saber que se leyo correctamente la placa
        messagebox.showinfo(message='PLACA leída "CORRECTAMENTE"')
        # Se lee la fecha y la hora
        date_=datetime.today().strftime('%x %X') #leer le fecha y hora
        fechas.append(date_.split(' '))
        #date,time_=date_[0], date_[1]

    else: # cuando la imagen no tenga una placa
        messagebox.showerror("ERROR", 'La imagen NO FUE RECONOCIDA')

'''
def validar_placa(data, lista_placas,max_num_plates, fechas):
    if data['results'] !=[]: #siempre que la imagen sea una placa
        #validar que no haya placas repetidas
        for result in data['results']:
            license_plate=result['plate']
            print(license_plate)
        if len(lista_placas)>1: #para comenzar a verificar a partir de 1 placa
            if result['plate'] in lista_placas:
                print(result['plate'])
                validar=1 #filtra placas repetidas
            else:
                lista_placas.append(license_plate)
                validar=0 #asigna cero si no esta repetida
                
        else: # se agrega la primera placa
            lista_placas.append(license_plate)
            validar=0
            
        #verificar que haya menos de maximo numero de carros
        if len(lista_placas)>max_num_plates:
            validar=2
            lista_placas.pop() #elimina la ultima placa de la lista
        # se verifica si hay espacio en el parking o si esta repetida la placa antes de guardar
        guardar_placa3(data,validar)
        # Mensaje para saber que se leyo correctamente la placa
        messagebox.showinfo(message='PLACA leída "CORRECTAMENTE"')
        # Se lee la fecha y la hora
        '''
        date_=datetime.today().strftime('%x %X') #leer le fecha y hora
        fechas.append(date_.split(' '))
        #date,time_=date_[0], date_[1]
        '''
    else: # cuando la imagen no tenga una placa
        messagebox.showerror("ERROR", 'La imagen NO FUE RECONOCIDA')



'''
#funcion para ir guardando en el archivo cada placa leida
def guardar_placa(data, file, writer,validar):
        for result in data['results']:
            if validar==0:
                writer.writerow(dict(date=datetime.today().strftime('%x %X'),
                         license_plate=result['plate'],
                         score=result['score'],
                         dscore=result['dscore'],
                         ))
            if validar==1:
                messagebox.showerror("ERROR", 'PLACA REPETIDA')
            if validar==2:
                messagebox.showerror("ERROR", 'MAS de ' + str(max_num_plate)+ ' AUTOS')
'''        

def guardar_placa3(data,validar):
    conn = psycopg2.connect(
    host="localhost",
    database="vehiculos",
    user="postgres",
    password="monse29"
)
    print(validar)
    for result in data['results']:
            if validar==0:
                cur = conn.cursor() 
                for result in data['results']:
                    numero_matricula=result['plate']
                    fecha_actual = datetime.date.today()
                    hora_actual = datetime.datetime.now().time()
                    cur.execute("INSERT INTO entrada (matricula, fecha, hora) VALUES (%s, %s, %s)", (numero_matricula, fecha_actual, hora_actual))
                    conn.commit()
    # Cerrar la conexión a la base de datos
                    conn.close()
            if validar==1:
                messagebox.showerror("ERROR", 'PLACA REPETIDA')
            if validar==2:
                messagebox.showerror("ERROR", 'MAS de ' + str(max_num_plate)+ ' AUTOS')
# Crear una tabla para almacenar los datos de matrícula
    


'''
#Guardar en la base de datos
def guardar_placa2(data):
    conn = psycopg2.connect(
    host="localhost",
    database="vehiculos",
    user="postgres",
    password="monse29"
)
# Crear una tabla para almacenar los datos de matrícula
    cur = conn.cursor() 
    for result in data['results']:
        numero_matricula=result['plate']
    fecha_actual = datetime.date.today()
    hora_actual = datetime.datetime.now().time()
    cur.execute("INSERT INTO entrada (matricula, fecha, hora) VALUES (%s, %s, %s)", (numero_matricula, fecha_actual, hora_actual))
    conn.commit()
    # Cerrar la conexión a la base de datos
    conn.close()

'''


"""
Demo program that read plate
"""
#ventana principal

def ventana():
    
    sg.ChangeLookAndFeel('Black')
    # define the window layout
    conn = psycopg2.connect(
    host="localhost",
    database="vehiculos",
    user="postgres",
    password="monse29"
)
    cur = conn.cursor()
    cur.execute("SELECT matricula, fecha, hora FROM entrada")
    rows = cur.fetchall()
    layout = [[sg.Text('Lectura de Matriculas', size=(40, 1), justification='center', font='Helvetica 20')],
            [sg.Image(filename='', key='image'), 
            sg.Table(values=rows, headings=["Número de matrícula", "Fecha", "Hora"], auto_size_columns=True,
              justification='right', num_rows=min(len(rows), 20)),
             
             ],
            [sg.Button('Read plate', size=(10, 1), font='Helvetica 14'),
            sg.Button('Nuevo Vehiculo', size=(15, 1), font='Any 14'),
            sg.Button('Exit', size=(10, 1), font='Helvetica 14'),
            
            ]]
    # create the window and show it without the plot
    window = sg.Window('CONTROL-CHECK', layout)
    ######## ciclo para que la ventana se ejecute constantemente #######
    # se bare el archivo para escritura
    lista_placas=[]; fechas=[]
    # se hace un ciclo para que la ventana se mantenga constantemente
    # ---===--- Event LOOP Read and display frames, operate the GUI --- #
    cap = cv2.VideoCapture(0)

    recording = True
    while True:
        event, values = window.Read(timeout=20)
        if event == 'Exit' or event is None:
            break
        elif event == 'Read plate':
            foto="temp.jpg" # nombre de la imagen temporal a guardar
            # se guarda la imagen capturada por el video
            cv2.imwrite(foto,frame)
            # se llama a la funcion leer placa
            data=leer_placa(foto)
            print(data)
            #guardar_placa2(data)
            validar_placa(data,lista_placas,max_num_plate, fechas) #recibe la fecha y hora
            #validar_placa(data,lista_placas,file,writer,max_num_plate, fechas) #recibe la fecha y hora
        elif event == 'Nuevo Vehiculo':
            registro()
            
        if recording:
            ret, frame = cap.read()
            #Crear tablero de resultados
            frame = cv2.resize(frame, (800, 600))
            imgbytes = cv2.imencode('.png', frame)[1].tobytes()
            window.FindElement('image').Update(data=imgbytes)
    window.close() #cerrar todo

    

