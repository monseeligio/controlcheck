from tkinter import *
import os
from PIL import Image, ImageDraw, ImageFont


def registrarUsuario():
    global ventanaRUsuario
    global fondo2
    global titulo
    fondo2=PhotoImage(file="./imagenes/fondo2.png")
    ventanaRUsuario = Toplevel()
    ventanaRUsuario.title("REGISTRAR USUARIO")
    ventanaRUsuario.geometry("800x700")
    Label(ventanaRUsuario, image=fondo2).place(x=0, y=0) 
    titulo=PhotoImage(file="./imagenes/titulo.png")
    global idUsuario
    global apellidop
    global apellidom
    global nombre
    global role
    global password
    
    idUsuario= StringVar()
    apellidop= StringVar()
    apellidom= StringVar()
    nombre= StringVar()
    role= StringVar()
    password=StringVar()
    
    Label(ventanaRUsuario, image=titulo).place(x=370,y=15)

    Label(ventanaRUsuario, text="ID USUARIO:", background="white", font=("Helvetica", 12, "bold")).place(x=300,y=150)
    entrada_matricula = Entry(ventanaRUsuario, textvariable=idUsuario, bd=0,  highlightthickness=3, width=30) #ESPACIO PARA INTRODUCIR EL NOMBRE.
    entrada_matricula.place(x=420,y=150)
    
    
    Label(ventanaRUsuario, text="APELLIDO PATERNO:", background="white", font=("Helvetica", 12, "bold")).place(x=300,y=200)
    entrada_tipo = Entry(ventanaRUsuario, textvariable=apellidop, bd=0,  highlightthickness=3, width=30) #ESPACIO PARA INTRODUCIR EL NOMBRE.
    entrada_tipo.place(x=420,y=200)
    
    Label(ventanaRUsuario, text="APELLIDO MATERNO:", background="white", font=("Helvetica", 12, "bold")).place(x=300,y=250)
    entrada_marca = Entry(ventanaRUsuario, textvariable=apellidom, bd=0,  highlightthickness=3, width=30) #ESPACIO PARA INTRODUCIR EL NOMBRE.
    entrada_marca.place(x=420,y=250)
    
    Label(ventanaRUsuario, text="ROLE:" , background="white", font=("Helvetica", 12, "bold")).place(x=300,y=300)
    entrada_modelo = Entry(ventanaRUsuario, textvariable=role, bd=0,  highlightthickness=3, width=30) #ESPACIO PARA INTRODUCIR EL NOMBRE.
    entrada_modelo.place(x=420,y=300)
    
    Label(ventanaRUsuario, text="PASSWORD:", background="white", font=("Helvetica", 12, "bold")).place(x=300,y=350)
    entrada_color = Entry(ventanaRUsuario, textvariable=password, bd=0,  highlightthickness=3, width=30) #ESPACIO PARA INTRODUCIR EL NOMBRE.
    entrada_color.place(x=420,y=350)
    Button(ventanaRUsuario, text="Registrarse", height="2", width="30").place(x=340,y=450)
    Button(ventanaRUsuario, text="Salir", height="2", width="30", command=Regresar).place(x=340,y=500)

def Regresar():
    ventanaRUsuario.withdraw()
