import tkinter as tk

class VentanaPrincipal:
    def __init__(self, master):
        self.master = master
        self.frame = tk.Frame(self.master)
        self.btn_abrir_ventana_secundaria = tk.Button(self.frame, text='Abrir ventana secundaria', command=self.abrir_ventana_secundaria)
        self.btn_abrir_ventana_secundaria.pack()
        self.frame.pack()

    def abrir_ventana_secundaria(self):
        self.new_window = tk.Toplevel(self.master)
        self.app = VentanaSecundaria(self.new_window, self.master)

class VentanaSecundaria:
    def __init__(self, master, ventana_principal):
        self.master = master
        self.ventana_principal = ventana_principal
        self.frame = tk.Frame(self.master)
        self.btn_cerrar_ventanas_y_abrir_principal = tk.Button(self.frame, text='Cerrar ventanas y volver a abrir la principal', command=self.cerrar_ventanas_y_abrir_principal)
        self.btn_cerrar_ventanas_y_abrir_principal.pack()
        self.frame.pack()

    def cerrar_ventanas_y_abrir_principal(self):
        self.master.destroy()
        self.ventana_principal.destroy()
        root = tk.Tk()
        app = VentanaPrincipal(root)
        root.mainloop()

def main():
    root = tk.Tk()
    app = VentanaPrincipal(root)
    root.mainloop()

if __name__ == '__main__':
    main()
