from tkinter import *
import os
from PIL import Image, ImageDraw, ImageFont


def registro():
    global ventana_registro
    global matriculaN
    global tipoN
    global marcaN
    global modeloN
    global colorN
    global fondo4
    ventana_registro = Toplevel()
    ventana_registro.title("Registro")
    ventana_registro.geometry("800x700")
    fondo4=PhotoImage(file="./imagenes/fondo4.png")
    Label(ventana_registro, image=fondo4).place(x=0, y=0) 
    matriculaN= StringVar()
    tipoN= StringVar()
    marcaN= StringVar()
    modeloN= StringVar()
    colorN= StringVar()
    var=IntVar()

    Label(ventana_registro, text="REGISTRO", background="white", font=("Helvetica", 20, "bold")).place(x=370,y=15)
    etiqueta_nombre = Label(ventana_registro, text="PERFIL", background="white", font=("Helvetica", 12, "bold"))
    etiqueta_nombre.place(x=410,y=65)
    
    Radiobutton(ventana_registro, text="Estudiantes", background="white",font=("Helvetica", 11, "bold"),  variable=var, value=1).place(x=300,y=90)
    Radiobutton(ventana_registro, text="Personal", background="white", font=("Helvetica", 11, "bold"),  variable=var, value=2).place(x=450,y=90)
    
    Label(ventana_registro, text="MATRICULA:", background="white", font=("Helvetica", 12, "bold")).place(x=300,y=150)
    entrada_matricula = Entry(ventana_registro, textvariable=matriculaN, bd=0,  highlightthickness=3, width=30) #ESPACIO PARA INTRODUCIR EL NOMBRE.
    entrada_matricula.place(x=420,y=150)
    
    
    Label(ventana_registro, text="TIPO:", background="white", font=("Helvetica", 12, "bold")).place(x=300,y=200)
    entrada_tipo = Entry(ventana_registro, textvariable=tipoN, bd=0,  highlightthickness=3, width=30) #ESPACIO PARA INTRODUCIR EL NOMBRE.
    entrada_tipo.place(x=420,y=200)
    
    Label(ventana_registro, text="MARCA:", background="white", font=("Helvetica", 12, "bold")).place(x=300,y=250)
    entrada_marca = Entry(ventana_registro, textvariable=marcaN, bd=0,  highlightthickness=3, width=30) #ESPACIO PARA INTRODUCIR EL NOMBRE.
    entrada_marca.place(x=420,y=250)
    
    Label(ventana_registro, text="MODELO:" , background="white", font=("Helvetica", 12, "bold")).place(x=300,y=300)
    entrada_modelo = Entry(ventana_registro, textvariable=modeloN, bd=0,  highlightthickness=3, width=30) #ESPACIO PARA INTRODUCIR EL NOMBRE.
    entrada_modelo.place(x=420,y=300)
    
    Label(ventana_registro, text="COLOR:", background="white", font=("Helvetica", 12, "bold")).place(x=300,y=350)
    entrada_color = Entry(ventana_registro, textvariable=colorN, bd=0,  highlightthickness=3, width=30) #ESPACIO PARA INTRODUCIR EL NOMBRE.
    entrada_color.place(x=420,y=350)
    
    Button(ventana_registro, text="Registrarse", height="2", width="30").place(x=340,y=450)
    Button(ventana_registro, text="Salir", height="2", width="30", command=Salir).place(x=340,y=500)
    

def Salir():
    ventana_registro.withdraw()