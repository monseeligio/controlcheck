from tkinter import *
import os
from PIL import Image, ImageDraw, ImageFont
from demo_v2_gui import ventana
from RegistroUsuario import registrarUsuario

def login():
    global fondo2
    global ventana_login
    fondo2=PhotoImage(file="./imagenes/fondo2.png")
    ventana_login = Toplevel()
    ventana_login.geometry("800x700")#DIMENSIONES DE LA VENTANA
    ventana_login.title("LOGIN")#TITULO DE LA VENTANA
    ventana_login.resizable(False, False)#NO SE VA REDIMENSIONAR, TENDRA UNA MEDIDA AJUSTADA
    Label(ventana_login, image=fondo2).place(x=0, y=0) 
    Label(ventana_login, text="").pack()
    Label(ventana_login, text="").pack()
    Label(ventana_login, text="").pack()
    Label(ventana_login, text="").pack()
    Label(ventana_login, text="").pack()

    Label(ventana_login, text="HOLA",  font=("Helvetica", 30, "bold")).pack()
    Label(ventana_login, text="").pack()

    Label(ventana_login, text="Introduzca nombre de usuario y contraseña",  font=("Helvetica", 16, "bold")).pack()
    Label(ventana_login, text="").pack()
    
    global verifica_usuario
    global verifica_clave
    verifica_usuario = StringVar()
    verifica_clave = StringVar()
    global entrada_login_usuario
    global entrada_login_clave
    Label(ventana_login, text="Nombre usuario * ",  font=("Helvetica", 12, "bold")).pack()
    Label(ventana_login, text="").pack()

    entrada_login_usuario = Entry(ventana_login, textvariable=verifica_usuario)
    entrada_login_usuario.pack()
    Label(ventana_login, text="").pack()
    Label(ventana_login, text="Contraseña * " , font=("Helvetica", 12, "bold")).pack()
    entrada_login_clave = Entry(ventana_login, textvariable=verifica_clave, show= '*')
    Label(ventana_login, text="").pack()

    entrada_login_clave.pack()
    Label(ventana_login, text="").pack()
    Button(ventana_login, text="Acceder", width=20, height=1, command=IngresoPrincipal).pack()
    Label(ventana_login, text="").pack()
    Button(ventana_login, text="Registrar usuario", width=20, height=1, command=IngresoRegistroUsuario).pack()


def IngresoPrincipal():
    ventana_login.destroy()
    ventana()
    
def IngresoRegistroUsuario():
    ventana_login.destroy()
    registrarUsuario()
 
