from tkinter import *
import os
from PIL import Image, ImageDraw, ImageFont
from RegistroVehiculo import registro
from RegistroVehiculo import registro

#ventana de eleccion
def eleccion():
    global fondo3
    global ventana_Eleccion
    fondo3=PhotoImage(file="./imagenes/registro.png")
    ventana_Eleccion = Toplevel()
    ventana_Eleccion.title("Registro")
    ventana_Eleccion.geometry("800x700")#DIMENSIONES DE LA VENTANA
    ventana_Eleccion.resizable(False, False)#NO SE VA REDIMENSIONAR, TENDRA UNA MEDIDA AJUSTADA
    Label(ventana_Eleccion, image=fondo3).place(x=0, y=0) 
    Button(ventana_Eleccion, text="Motocicleta", height="2", width="10",  command=IngresoRegistro).place(x=160,y=560)#
    Button( ventana_Eleccion, text="Automovíl", height="2", width="10", command=IngresoRegistro).place(x=600,y=560)#

def IngresoRegistro():
    ventana_Eleccion.withdraw()
    registro()
    