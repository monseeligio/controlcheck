#como generar una ventanaa de login conectada con pagina principal?
#CREANDO LOGIN CON PYTHON Y TKINTER.

#IMPORTAMOS LIBRERÍAS NECESARIAS.
from tkinter import *
import os
from PIL import Image, ImageDraw, ImageFont
from Log import login
from RegistroUsuario import registrarUsuario


#CREAMOS VENTANA PRINCIPAL.
def ventana_inicio():
    global ventana_principal
    global fondo
    global fondo2
    global fondo3
    global fondo4
    global titulo
    pestas_color="DarkGrey"
    ventana_principal=Tk()
    ventana_principal.geometry("800x700")#DIMENSIONES DE LA VENTANA
    ventana_principal.title("BIENVENIDO")#TITULO DE LA VENTANA
    ventana_principal.resizable(False, False)#NO SE VA REDIMENSIONAR, TENDRA UNA MEDIDA AJUSTADA
    fondo=PhotoImage(file="./imagenes/fondo2.png")
    fondo2=PhotoImage(file="./imagenes/fondo2.png")
    fondo3=PhotoImage(file="./imagenes/registro.png")
    fondo4=PhotoImage(file="./imagenes/fondo4.png")
    titulo=PhotoImage(file="./imagenes/titulo.png")
    
    Label(image=fondo).place(x=0, y=0) 
    Label( text="BIENVENIDO", font=("Helvetica", 24, "bold")).place(x=315,y=120)
    Button(text="Iniciar sesión", height="2", width="30", bg=pestas_color, command=IngresoLogin).place(x=300,y=250)#
    Button(text="Salir", height="2", width="30", bg=pestas_color, command=exit).place(x=300,y=300)#

    ventana_principal.mainloop()
    


'''
def login():
    global ventana_login
    ventana_login = Toplevel()
    ventana_login.geometry("800x700")#DIMENSIONES DE LA VENTANA
    ventana_login.title("LOGIN")#TITULO DE LA VENTANA
    ventana_login.resizable(False, False)#NO SE VA REDIMENSIONAR, TENDRA UNA MEDIDA AJUSTADA
    Label(ventana_login, image=fondo2).place(x=0, y=0) 
    Label(ventana_login, text="").pack()
    Label(ventana_login, text="").pack()
    Label(ventana_login, text="").pack()
    Label(ventana_login, text="").pack()
    Label(ventana_login, text="HOLA",  font=("Helvetica", 30, "bold")).pack()
    Label(ventana_login, text="").pack()

    Label(ventana_login, text="Introduzca nombre de usuario y contraseña",  font=("Helvetica", 16, "bold")).pack()
    Label(ventana_login, text="").pack()
    
    global verifica_usuario
    global verifica_clave
    verifica_usuario = StringVar()
    verifica_clave = StringVar()
    global entrada_login_usuario
    global entrada_login_clave
    Label(ventana_login, text="Nombre usuario * ",  font=("Helvetica", 12, "bold")).pack()
    entrada_login_usuario = Entry(ventana_login, textvariable=verifica_usuario)
    entrada_login_usuario.pack()
    Label(ventana_login, text="").pack()
    Label(ventana_login, text="Contraseña * " , font=("Helvetica", 12, "bold")).pack()
    entrada_login_clave = Entry(ventana_login, textvariable=verifica_clave, show= '*')
    entrada_login_clave.pack()
    Label(ventana_login, text="").pack()
    Button(ventana_login, text="Acceder", width=10, height=1, command=Ingreso).pack()

#ventana de eleccion
def eleccion():
    global ventana_Eleccion
    ventana_Eleccion = Toplevel()
    ventana_Eleccion.title("Registro")
    ventana_Eleccion.geometry("800x700")#DIMENSIONES DE LA VENTANA
    ventana_Eleccion.resizable(False, False)#NO SE VA REDIMENSIONAR, TENDRA UNA MEDIDA AJUSTADA
    Label(ventana_Eleccion, image=fondo3).place(x=0, y=0) 
    Button(ventana_Eleccion, text="Motocicleta", height="2", width="10", command=registro).place(x=160,y=560)#
    Button( ventana_Eleccion, text="Automovíl", height="2", width="10", command=registro).place(x=600,y=560)#
    
    
#CREAMOS VENTANA PARA REGISTRO.
def registro():
    global ventana_registro
    ventana_registro = Toplevel(ventana_principal)
    ventana_registro.title("Registro")
    ventana_registro.geometry("800x700")
    Label(ventana_registro, image=fondo4).place(x=0, y=0) 
    global matriculaN
    global tipoN
    global marcaN
    global modeloN
    global colorN
    matriculaN= StringVar()
    tipoN= StringVar()
    marcaN= StringVar()
    modeloN= StringVar()
    colorN= StringVar()
    var=IntVar()
    
    Label(ventana_registro, text="REGISTRO", background="white", font=("Helvetica", 20, "bold")).place(x=370,y=15)
    etiqueta_nombre = Label(ventana_registro, text="PERFIL", background="white", font=("Helvetica", 12, "bold"))
    etiqueta_nombre.place(x=410,y=65)
    
    Radiobutton(ventana_registro, text="Estudiantes", background="white",font=("Helvetica", 11, "bold"),  variable=var, value=1).place(x=300,y=90)
    Radiobutton(ventana_registro, text="Personal", background="white", font=("Helvetica", 11, "bold"),  variable=var, value=2).place(x=450,y=90)
    
    Label(ventana_registro, text="MATRICULA:", background="white", font=("Helvetica", 12, "bold")).place(x=300,y=150)
    entrada_matricula = Entry(ventana_registro, textvariable=matriculaN, bd=0,  highlightthickness=3, width=30) #ESPACIO PARA INTRODUCIR EL NOMBRE.
    entrada_matricula.place(x=420,y=150)
    
    
    Label(ventana_registro, text="TIPO:", background="white", font=("Helvetica", 12, "bold")).place(x=300,y=200)
    entrada_tipo = Entry(ventana_registro, textvariable=tipoN, bd=0,  highlightthickness=3, width=30) #ESPACIO PARA INTRODUCIR EL NOMBRE.
    entrada_tipo.place(x=420,y=200)
    
    Label(ventana_registro, text="MARCA:", background="white", font=("Helvetica", 12, "bold")).place(x=300,y=250)
    entrada_marca = Entry(ventana_registro, textvariable=marcaN, bd=0,  highlightthickness=3, width=30) #ESPACIO PARA INTRODUCIR EL NOMBRE.
    entrada_marca.place(x=420,y=250)
    
    Label(ventana_registro, text="MODELO:" , background="white", font=("Helvetica", 12, "bold")).place(x=300,y=300)
    entrada_modelo = Entry(ventana_registro, textvariable=modeloN, bd=0,  highlightthickness=3, width=30) #ESPACIO PARA INTRODUCIR EL NOMBRE.
    entrada_modelo.place(x=420,y=300)
    
    Label(ventana_registro, text="COLOR:", background="white", font=("Helvetica", 12, "bold")).place(x=300,y=350)
    entrada_color = Entry(ventana_registro, textvariable=colorN, bd=0,  highlightthickness=3, width=30) #ESPACIO PARA INTRODUCIR EL NOMBRE.
    entrada_color.place(x=420,y=350)
    
    #Button(ventana_registro, text="Registrarse", height="2", width="30", command = registro_usuario).place(x=340,y=450)
    

    
    
def registrarUsuario():
    global ventanaRUsuario
    ventanaRUsuario = Toplevel(ventana_principal)
    ventanaRUsuario.title("REGISTRAR USUARIO")
    ventanaRUsuario.geometry("800x700")
    Label(ventanaRUsuario, image=fondo2).place(x=0, y=0) 
    global idUsuario
    global apellidop
    global apellidom
    global nombre
    global role
    global password
    
    idUsuario= StringVar()
    apellidop= StringVar()
    apellidom= StringVar()
    nombre= StringVar()
    role= StringVar()
    password=StringVar()
    
    Label(ventanaRUsuario, image=titulo).place(x=370,y=15)

    Label(ventanaRUsuario, text="ID USUARIO:", background="white", font=("Helvetica", 12, "bold")).place(x=300,y=150)
    entrada_matricula = Entry(ventanaRUsuario, textvariable=idUsuario, bd=0,  highlightthickness=3, width=30) #ESPACIO PARA INTRODUCIR EL NOMBRE.
    entrada_matricula.place(x=420,y=150)
    
    
    Label(ventanaRUsuario, text="APELLIDO PATERNO:", background="white", font=("Helvetica", 12, "bold")).place(x=300,y=200)
    entrada_tipo = Entry(ventanaRUsuario, textvariable=apellidop, bd=0,  highlightthickness=3, width=30) #ESPACIO PARA INTRODUCIR EL NOMBRE.
    entrada_tipo.place(x=420,y=200)
    
    Label(ventanaRUsuario, text="APELLIDO MATERNO:", background="white", font=("Helvetica", 12, "bold")).place(x=300,y=250)
    entrada_marca = Entry(ventanaRUsuario, textvariable=apellidom, bd=0,  highlightthickness=3, width=30) #ESPACIO PARA INTRODUCIR EL NOMBRE.
    entrada_marca.place(x=420,y=250)
    
    Label(ventanaRUsuario, text="ROLE:" , background="white", font=("Helvetica", 12, "bold")).place(x=300,y=300)
    entrada_modelo = Entry(ventanaRUsuario, textvariable=role, bd=0,  highlightthickness=3, width=30) #ESPACIO PARA INTRODUCIR EL NOMBRE.
    entrada_modelo.place(x=420,y=300)
    
    Label(ventanaRUsuario, text="PASSWORD:", background="white", font=("Helvetica", 12, "bold")).place(x=300,y=350)
    entrada_color = Entry(ventanaRUsuario, textvariable=password, bd=0,  highlightthickness=3, width=30) #ESPACIO PARA INTRODUCIR EL NOMBRE.
    entrada_color.place(x=420,y=350)
    
    #Button(ventanaRUsuario, text="Registrarse", height="2", width="30", command = registro_usuario).place(x=340,y=450)
    

#VENTANA "VERIFICACION DE LOGIN".


def verifica_login():
    usuario1 = verifica_usuario.get()
    clave1 = verifica_clave.get()
    entrada_login_usuario.delete(0, END) #BORRA INFORMACIÓN DEL CAMPO "Nombre usuario *" AL MOSTRAR NUEVA VENTANA.
    entrada_login_clave.delete(0, END) #BORRA INFORMACIÓN DEL CAMPO "Contraseña *" AL MOSTRAR NUEVA VENTANA.
 
    lista_archivos = os.listdir() #GENERA LISTA DE ARCHIVOS UBICADOS EN EL DIRECTORIO.
    #SI EL NOMBRE SE ENCUENTRA EN LA LISTA DE ARCHIVOS..
    if usuario1 in lista_archivos:
        archivo1 = open(usuario1, "r") #APERTURA DE ARCHIVO EN MODO LECTURA
        verifica = archivo1.read().splitlines() #LECTURA DEL ARCHIVO QUE CONTIENE EL nombre Y contraseña.
        #SI LA CONTRASEÑA INTRODUCIDA SE ENCUENTRA EN EL ARCHIVO...
        if clave1 in verifica:
            exito_login() #...EJECUTAR FUNCIÓN "exito_login()"
        #SI LA CONTRASEÑA NO SE ENCUENTRA EN EL ARCHIVO....
        else:
            no_clave() #...EJECUTAR "no_clave()"
    #SI EL NOMBRE INTRODUCIDO NO SE ENCUENTRA EN EL DIRECTORIO...
    else:
        no_usuario() #..EJECUTA "no_usuario()".




# VENTANA "Login finalizado con exito".
 
def exito_login():
    global ventana_exito
    ventana_exito = Toplevel(ventana_login)
    ventana_exito.title("Exito")
    ventana_exito.geometry("150x100")
    Label(ventana_exito, text="Login finalizado con exito").pack()
    Button(ventana_exito, text="OK").pack()
 
#VENTANA DE "Contraseña incorrecta".
 
def no_clave():
    global ventana_no_clave
    ventana_no_clave = Toplevel(ventana_login)
    ventana_no_clave.title("ERROR")
    ventana_no_clave.geometry("150x100")
    Label(ventana_no_clave, text="Contraseña incorrecta ").pack()
    Button(ventana_no_clave, text="OK").pack() #EJECUTA "borrar_no_clave()".
 
#VENTANA DE "Usuario no encontrado".
 
def no_usuario():
    global ventana_no_usuario
    ventana_no_usuario = Toplevel(ventana_login)
    ventana_no_usuario.title("ERROR")
    ventana_no_usuario.geometry("150x100")
    Label(ventana_no_usuario, text="Usuario no encontrado").pack()
    Button(ventana_no_usuario, text="OK", command=borrar_no_usuario).pack() #EJECUTA "borrar_no_usuario()"
'''
#CERRADO DE VENTANAS

def IngresoLogin():
    login()

 
'''   
def Ingreso():
    ventana_login.withdraw()
    ventana()
 
 
def borrar_no_usuario():
    ventana_no_usuario.destroy()
'''
#REGISTRO USUARIO

ventana_inicio()  #EJECUCIÓN DE LA VENTANA DE INICIO.

